(ns balances.model.account
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [balances.dateutil :as dtutil]))

; initialize data from db
(def json-accounts
  (json/read-json (slurp (io/resource "accounts.json"))))

(log/debug "Loading accounts:" json-accounts)

(def accounts-ref (ref {}))

; boilerplate to work with ref
(defn get-accounts-ref []
  (deref accounts-ref))

(defn set-accounts-ref [val]
  (dosync (ref-set accounts-ref val)))

(defn init-account-ref []
  (set-accounts-ref
   (group-by :id (get json-accounts :accounts))))

(defn update-accounts-ref [id accnt]
  (set-accounts-ref
   (assoc (get-accounts-ref) id (conj [] accnt))))

(init-account-ref)

(log/debug "Accounts loaded:" (get-accounts-ref))

; helper functions
(defn gen-uuid [] (subs (str (java.util.UUID/randomUUID)) 0 8))

(defn gen-seq [accnt] (count (get accnt :txns)))

(defn add-txn ([accnt date desc amount]
        ; TODO validate inputs
               (let [new-txn {:_id (str (gen-seq accnt))
                              :date (str date)
                              :desc (str desc)
                              :amount amount}]
                 (assoc accnt :txns (conj (:txns accnt) new-txn)))))

(defn to-list [v map]
  (for [keyval map]
    (assoc v (key keyval) (first (val keyval)))))

(defn do-get "@return Account(s) in memory."
  ([] (to-list {} (get-accounts-ref)))
  ([id] (first (get (get-accounts-ref) id))))

(defn get-operations "@return Transactions for the specified account `id`." [id]
  (sort-by :date (get (do-get id) :txns))
)

; FIXME alter instead of ref-set for concurrent updates
(defn do-update "Includes a new operation in the transaction list." [id date desc amount]
  (first (get (update-accounts-ref id
                                   (add-txn (do-get id) date desc amount)) id)))

(defn do-balance [txns]
  (reduce +
          (for [txn txns]
            (get txn :amount))))

(defn do-statement [txns from to]
  (vec (filter #(dtutil/between (get-in % [:date]) from to) txns)))

(defn get-balance "@return Current balance for the specified account `id`."
  ([id] (hash-map :id id :balance (do-balance (get-operations id))))
  ([id to] (do-balance (do-statement (get-operations id) (dtutil/instant 0) to))))

; balance of all operations until each date
; FIXME Replace with a proper algorithm e.g. recalculating on each txn
(defn do-balance-by-date [id txns_by_date]
  (into {}
   (let [m []]
     (for [date txns_by_date]
       (conj m (key date) (get-balance id (dtutil/str-date (key date)))) ; {date: balance until date}
     ))))

(defn get-statement "@return List of operations for the specified period (between `from` and `to`)." [id from to]
  (hash-map :id id :balance (get-balance id to) :statement (do-statement (get-operations id) from to)))

; period when operations are present
(defn get-operations-by-date [txns]
  (group-by :date
    (for [txn txns]
      (assoc txn :date (dtutil/str-local-date (:date txn) )))))

(defn get-daily-balance "@return Daily balances for the specified account `id`." [id]
  (do-balance-by-date id (get-operations-by-date (get-operations id))))

; TODO Procedural code, not functional
(defn populate-positive-dates [b]
   (let [m []]
     (for [date_balance b]
      (cond
        (>= (get date_balance 1) 0) (conj m (first date_balance))
       )
    )
    ))

(defn do-end-date [start b current_balance]
 (format "Negative balance: %s. Start: %s - End: %s" current_balance start
  (first (first
    (filter (fn [end-date]
      (dtutil/is-after (first end-date) start)) ; filter the closest "non-default" date from 'b' after
      (populate-positive-dates b)))
)))

(defn report-negative-by-date [b]
    (for [date_balance b]
      (cond
        (neg? (get date_balance 1))
          (do-end-date (first date_balance) b (get date_balance 1))
            :else (format "Positive balance: %s. Start: %s" (get date_balance 1) (first date_balance))
       )
    )
)

(defn get-negative-balance "@return Negative balance report" [id]
  (report-negative-by-date (get-daily-balance id)))