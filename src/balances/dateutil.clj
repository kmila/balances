(ns balances.dateutil
  (:require [clojure.tools.logging :as log]
    [java-time]))

(defn iso-date [dt]
  (java-time/local-date-time "yyyy-MM-dd'T'HH:mm:ss'Z'" dt))

(defn str-date [dt] (str dt "T23:59:59Z"))
(defn str-local-date [dt] (str (java-time/local-date "yyyy-MM-dd'T'HH:mm:ss'Z'" dt)))

(defn between [v from to]
  (print (iso-date from))
  (and
   (.isAfter (iso-date v) (iso-date from))
   (.isBefore (iso-date v) (iso-date to))))

(defn instant
  ([] (str (java-time/instant)))
  ([ts] (str (java-time/instant ts))))

(defn ymd-date [dt]
  (java-time/local-date "yyyy-MM-dd" dt))

(defn is-after [lhs rhs]
  (and
    (some? lhs)
    (some? rhs)
    (.isAfter (ymd-date lhs) (ymd-date rhs))
  ))