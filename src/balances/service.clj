(ns balances.service
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [cheshire.core :as json]
            [balances.model.account :as account]))

(defn get-accounts
  [request]
  (http/json-response (account/do-get)))

(defn get-account
  [request]
  (let [account-number (get-in request [:path-params :account-number])]
    (http/json-response (account/do-get account-number))))

(defn update-account
  [request]
  (let [account-number (get-in request [:path-params :account-number]) transaction (:json-params request)]
    (ring-resp/content-type (ring-resp/created
                             "uri://account"
                             (json/generate-string
                              (account/do-update account-number (get transaction :date) (get transaction :desc) (get transaction :amount))))  "application/json")))

(defn get-balance
  [request]
  (let [account-number (get-in request [:path-params :account-number])]
    (http/json-response (account/get-balance account-number))))

(defn get-daily-balance
  [request]
  (let [account-number (get-in request [:path-params :account-number])]
    (http/json-response (account/get-daily-balance account-number))))

(defn statement
  [request]
  (let [account-number (get-in request [:path-params :account-number]) dt (:json-params request)]
    (ring-resp/content-type (ring-resp/created
                             "uri://statement"
                             (json/generate-string
                             (account/get-statement account-number (get dt :from) (get dt :to))))  "application/json")))

(defn get-negative-balance
  [request]
  (let [account-number (get-in request [:path-params :account-number])]
    (http/json-response (account/get-negative-balance account-number))))

(defn home-page
  [request]
  (ring-resp/response (format "Clojure %s - served from %s <br/>\n Operations: \n<ul><li> /accounts, \n</li><li> /account/ID (GET POST) /account/ID/balance (GET) /account/ID/balances (GET) /account/ID/negative (GET), \n</li><li> /statement/ID (POST)</li></ul>"
                              (clojure-version)
                              (route/url-for ::home-page))))

;; The interceptors defined after the verb map (e.g., {:get home-page}
;; apply to / and its children.
(def common-interceptors [(body-params/body-params) http/html-body])

;; Map-based routes
(def routes `{"/" {:interceptors [(body-params/body-params) http/html-body]
                   :get home-page
                   "/accounts" {:get get-accounts}
                   "/account/:account-number" {:get get-account :post update-account}
                   "/account/:account-number/balance" {:get get-balance}
                   "/account/:account-number/balances" {:get get-daily-balance}
                   "/account/:account-number/default" {:get get-negative-balance}
                   "/statement/:account-number" {:post statement}}})

;; Consumed by balances.server/create-server
(def service {:env :prod
              ::http/routes routes
              ::http/resource-path "/public"
              ::http/type :jetty
              ::http/port (Integer. (or (System/getenv "PORT") 8080))
              ::http/container-options {:h2c? true
                                        :h2? false
                                        ;:keystore "test/hp/keystore.jks"
                                        ;:key-password "password"
                                        ;:ssl-port 8443
                                        :ssl? false}})