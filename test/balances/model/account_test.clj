(ns balances.model.account-test
  (:require [clojure.test :refer :all]
            [io.pedestal.test :refer :all]
            [io.pedestal.http :as bootstrap]
            [cheshire.core :as json]
            [balances.dateutil :as dtutil]
            [balances.model.account :as account]))

(deftest do-get-test
  (testing "read all accounts"
    (is (= 5 (count (account/do-get)))))
  (testing "read account by id"
    (is (= "{\"id\":\"a4da19e8\",\"txns\":[{\"_id\":\"0\",\"amount\":0,\"date\":\"2017-12-01T12:00:00Z\",\"desc\":\"OPEN\"}]}"
           (json/generate-string (account/do-get "a4da19e8"))))))

(deftest add-txn-test
  (is (= -123
         (get (last
               (get
                (account/add-txn (account/do-get "a4da19e8") (dtutil/instant) "TEST DEBIT 1" -123)
                :txns)) :amount))))

;FIXME Stateful test
;(deftest do-update-test
;  (is (= 1 (count (get (account/do-get "a4da19e8") :txns))))
;  (account/do-update "a4da19e8" (dtutil/instant) "TEST UPDATE" 100)
;  (is (= 2 (count (get (account/do-get "a4da19e8") :txns)))))

(deftest get-balance-test
  (is (= 10500 (get (account/get-balance "c9c6872c") :balance))))
  (is (= 10500 (account/do-balance (account/do-statement (get (account/do-get "c9c6872c") :txns) "2016-10-15T12:00:00Z" "2018-10-17T12:00:00Z") )))

(deftest get-daily-balance-test
  (is (= -15000 (get (account/get-daily-balance "b0265bcc") "2017-12-02"))))

(deftest get-statement-test
  (account/get-statement "d12e456f" "2017-10-15T12:00:00Z" "2017-10-17T12:00:00Z")
  (is (= 95143 (account/get-balance "d12e456f" "2017-10-17T12:00:00Z"))))

(deftest get-negative-balance-test
  (account/get-negative-balance "e21d654c"))

(run-tests 'balances.model.account-test)